//
//  File.swift
//  
//
//  Created by Noah Kamara on 03.08.20.
//

import Foundation
import SwiftUI

public protocol IndicatorStatus: CaseIterable, Hashable, CustomStringConvertible {
    typealias Status = IndicatorStatus
    
    /// Status - Icon
    var view: AnyView { get }
    
    /// Status - Description
    var description: String { get }
    
    /// Resets the Status to .unknown
    mutating func invalidate()
}
