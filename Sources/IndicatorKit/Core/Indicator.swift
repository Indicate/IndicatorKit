//
//  Indicator.swift
//  
//
//  Created by Noah Kamara on 03.08.20.
//

import Foundation
import SwiftUI


public protocol Indicator: ObservableObject, Hashable, Identifiable {
    associatedtype Status: IndicatorStatus
    
    /// Identifier
    var id: UUID { get }
    
    /// Label
    var label: String { get }
    
    /// Color Map
    var colorMap: ColorMap<Status> { get }
    
    /// Status
    var status: Status { get }
    
    /// Color
    var color: Color { get }
    
    /// Refreshes the Status and returns it as completion
    func refresh(_ completion: @escaping (Status) -> Void)
    
    /// Refreshes the Status
    func refresh()
}

//public static func == (lhs: WeatherConditionIndicator, rhs: WeatherConditionIndicator) -> Bool {
//    return lhs.id == rhs.id
//}
//
//public func hash(into hasher: inout Hasher) {
//    return hasher.combine(self.id)
//}
//
//// Identifier
//private(set) public var id: UUID
//
///// Label
//private(set) public var label: String
//
///// Color Map
//private(set) public var colorMap: ColorMap<Status>
//
///// Status
//@Published private(set) public var status: Status = .unknown
//
///// Color
//public var color: Color {
//    return self.colorMap.color(for: self.status)
//}
//
///// Refreshes the Status and returns it as completion
//public func refresh(_ completion: @escaping (Status) -> Void) {
//    
//}
//
///// Refreshes the Status
//public func refresh() {
//    
//}
//
////MARK: Other Vars
//private let location: CLLocationCoordinate2D
//
//init(id: UUID = UUID(), label: String = "Example Label", colorMap: ColorMap<Status>, location: CLLocationCoordinate2D) {
//    self.id = id
//    self.label = label
//    self.colorMap = colorMap
//    self.location = location
//    self.status = .unknown
//}
