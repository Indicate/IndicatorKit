//
//  File.swift
//  
//
//  Created by Noah Kamara on 03.08.20.
//

import Foundation
import SwiftUI


/// ColorMap Class
public class ColorMap<Status: IndicatorStatus> {
    private var map: [Status: Color]
    private var ´default´: Color
    
    public func color(for status: Status) -> Color {
        if map.contains(where: {$0.key == status}) {
            return map[status] ?? ´default´
        } else {
            return ´default´
        }
    }
    
    public init(_ map: [Status: Color], default defaultValue: Color = .primary) {
        self.map = map
        self.´default´ = defaultValue
    }
}
