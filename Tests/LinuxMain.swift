import XCTest

import IndicatorKitTests

var tests = [XCTestCaseEntry]()
tests += IndicatorKitTests.allTests()
XCTMain(tests)
